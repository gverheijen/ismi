# Automated Measurement of Fetal HeadCircumference

## Description
This python notebook can be used to automatically calculate the ellipse parameters for images containing fetus heads. The notebook Fetal_head_measurement is the main notebook used in the project, and contains comments and notes on how to use it. To train new models, one can scroll down to the last couple of cells in the notebook for instructions on how to go about this. 

We wanted to include our pretrained models into the github as well, but sadly we ran into some issues with file size. This prevented us from doing so, and made it so that one would need to train their models themselves to use it. If you do want the saved model parameters, feel free to contact Gido using the email in the notebook. 

## Authors and acknowledgment
This notebook was made for the Intelligent Systems in Medical Imaging (Ismi) course at the radboud university in the year 2022-2023. The notebook was made as a collaboration between Luke van Leijenhorst, Julia Janssen, and Gido Verheijen. 

We would like to also give our thanks to Keelin Murphy and Sofía Sappia for their assistance with this project. 

